from django.shortcuts import render,redirect
from django.contrib.auth import login, authenticate,logout
from django.http import HttpResponse
# Create your views here.

def user_login(request):
    if request.user.is_authenticated:
        return redirect('halo/')
    else:
        if request.method == "GET": 
            return render(request, "login.html")
        elif request.method == "POST":
            username = request.POST['username']
            password = request.POST['password']
            user = authenticate(request, username=username, password=password)
            if user is not None:
                login(request,user)
                request.session['username'] = request.user.username
                return redirect('halo/')
            else:
                return redirect('/')

def halo(request):
    if not request.user.is_authenticated:
        return redirect('/')
    else:
        return render(request, "index.html")

def user_logout(request):
	logout(request)
	return redirect('/')
