from django.test import TestCase, Client
from django.urls import resolve
from django.http import HttpRequest
from django.contrib.auth.models import User
from .views import *

# Create your tests here.
class unitTest(TestCase):
	def test_login_url_is_exist(self):
		response = Client().get('/')
		self.assertEqual(response.status_code, 200)

	def test_using_right_template(self):
		response = Client().get('/')
		self.assertTemplateUsed(response, 'login.html')
	
	def test_using_right_staticfiles(self):
		response = Client().get('/')
		self.assertContains(response, 'static/css/style')

	def test_login_func(self):
		found = resolve('/')
		self.assertEqual(found.func, user_login)

	def test_welcome_after_login_redirect(self):
		user = User.objects.create_user('natel', 'natel@admin.com', 'justiniall')
		self.client.login(username='natel', password='justiniall')
		response = self.client.get('/')
		self.assertEqual(response.status_code, 302)

	def test_user_login(self):
		data = {'username' : 'natel', 'password' : 'justiniall'}
		response = self.client.post('/', data)
		self.assertEqual(response.status_code, 302)