from django.urls import path, include
from . import views
from django.contrib.auth import views as auth_views

urlpatterns = [
    path('', views.user_login, name="login"),
    path('halo/', views.halo, name="greetings"),
    path('logout/', views.user_logout, name="logout"),
]