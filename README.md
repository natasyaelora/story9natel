# Story 9
Natasya Elora Carissa
1806147086

# Pipeline and Coverage
[![pipeline status](https://gitlab.com/natasyaelora/story9natel/badges/master/pipeline.svg)](https://gitlab.com/natasyaelora/story9natel/commits/master)

[![coverage report](https://gitlab.com/natasyaelora/story9natel/badges/master/coverage.svg)](https://gitlab.com/natasyaelora/story9natel/commits/master)

# Herokuapp Link
http://story9natel.herokuapp.com/

